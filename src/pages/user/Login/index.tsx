import React, { useEffect } from 'react';
import { LoginForm } from '@ant-design/pro-form';
import { useIntl, SelectLang } from 'umi';
import Footer from '@/components/Footer';
import styles from './index.less';
import { useModel } from '@@/plugin-model/useModel';
import { history } from '@@/core/history';
import { message } from 'antd';



const WLogin = (params: API.WLoginParams) => {
  // @ts-ignore
  new WwLogin(params);
};

const Login: React.FC = () => {
  const { initialState } = useModel('@@initialState');
  if (initialState?.currentUser) {
    history.push('/');
  }
  const intl = useIntl();

  useEffect(() => {
    const defaultLoginInfoMessage = intl.formatMessage({
      id: 'pages.login.workwechat.info',
      defaultMessage: '请手机打开企业微信进行扫码登录！',
    });
    message.info(defaultLoginInfoMessage);
    WLogin({
      id: 'wx_reg',
      appid: 'wwa70dc5b6e56936e1',
      agentid: '1000002',
      redirect_uri: 'http://yepyih.natappfree.cc/test/foo2',
      state: 'x234faaf',
      href: 'https://openscrm.oss-cn-hangzhou.aliyuncs.com/public/qrcode.css',
      lang: 'zh',
    });
  });

  return (
    <div className={styles.container}>
      <div className={styles.lang} data-lang>
        {SelectLang && <SelectLang />}
      </div>
      <div className={styles.content}>
        <LoginForm
          logo={<img alt="logo" src="/logo.svg" />}
          title="牛X项目"
          subTitle={intl.formatMessage({ id: 'pages.layouts.userLayout.title' })}
          initialValues={{
            autoLogin: true,
          }}
          submitter={false}
        >
          <div id="wx_reg" style={{ textAlign: 'center' }}></div>
        </LoginForm>
      </div>
      <Footer />
    </div>
  );
};

export default Login;
